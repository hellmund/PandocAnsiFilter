pandoc -t pdf --ipynb-output=all --lua-filter=ansi2html.lua -s --katex Example1.ipynb  --pdf-engine=xelatex > Example1.pdf
pandoc -t html --ipynb-output=all --lua-filter=ansi2html.lua -s --katex Example1.ipynb  > Example1.html
